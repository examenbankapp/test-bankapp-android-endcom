package com.yader.bankapp.services;

import com.yader.bankapp.models.CuentaRespuesta;
import com.yader.bankapp.models.MovimientosRespuesta;
import com.yader.bankapp.models.SaldosRespuesta;
import com.yader.bankapp.models.TarjetaRespuesta;

import retrofit2.Call;
import retrofit2.http.GET;

public interface bankaApiService {
    @GET("tarjetas")
    Call<TarjetaRespuesta> obtenerListaTarjetas();

    @GET("movimientos")
    Call<MovimientosRespuesta> obtenerListaMovimientos();

    @GET("cuenta")
    Call<CuentaRespuesta> obtenerListaCuenta();

    @GET("saldos")
    Call<SaldosRespuesta> obtenerListaSaldos();

}
