package com.yader.bankapp.activities;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Paint;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.yader.bankapp.R;

public class MainActivity extends AppCompatActivity {

    private TextView txtMisCuentas, txtEnviarDinero, txtAdd;
    int REQUEST_CODE = 200;
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        verificarPermisos();
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.toolbar_bank);

        txtMisCuentas = findViewById(R.id.txt_mis_cuentas);
        txtEnviarDinero = findViewById(R.id.txt_enviar_dinero);
        txtAdd = findViewById(R.id.txt_add_tarjeta);

        txtAdd.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent intent = new Intent(MainActivity.this, AsociarTarjetaActivity.class);
                startActivity(intent);
            }
        });

        txtMisCuentas.setPaintFlags(txtAdd.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        txtEnviarDinero.setPaintFlags(txtAdd.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        txtAdd.setPaintFlags(txtAdd.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void verificarPermisos(){
        int permisoInternet = ContextCompat.checkSelfPermission(this, Manifest.permission.INTERNET);
        if (permisoInternet ==PackageManager.PERMISSION_GRANTED){

        }
        else{
            requestPermissions(new String[]{Manifest.permission.INTERNET}, REQUEST_CODE);
        }
        int permisoAccesNetwork = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_NETWORK_STATE);
        if (permisoAccesNetwork == PackageManager.PERMISSION_GRANTED){

        }
        else{
            requestPermissions(new String[]{Manifest.permission.ACCESS_NETWORK_STATE}, REQUEST_CODE);
        }
    }
}