package com.yader.bankapp.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.yader.bankapp.R;

public class AgregarTarjetaFragment extends Fragment {

    private TextView txtAdd;

    public static AgregarTarjetaFragment newInstance(String param1, String param2) {
        AgregarTarjetaFragment fragment = new AgregarTarjetaFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View vista = inflater.inflate(R.layout.fragment_agregar_tarjeta, container, false);
        return  vista;
    }

}